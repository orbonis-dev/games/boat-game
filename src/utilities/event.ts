export class Event<T> {
    private listeners: Array<(value: T) => void>;
    
    constructor() {
        this.listeners = [];
    }

    public add(listener: (value: T) => void): void {
        this.listeners.push(listener);
    }

    public remove(listener: (value: T) => void): void {
        const index: number = this.listeners.findIndex((x) => x === listener);
        if (index !== -1) {
            this.listeners.splice(index, 1);
        }
    }

    public dispatch(value: T): void {
        this.listeners.forEach((x) => x(value));
    }
}
