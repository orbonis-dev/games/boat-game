export const RandomString = (length: number, characterset?: string): string => {
    const characters: string = characterset ?? "abcdefghijklmnopqrstuvwxyz0123456789";
    let value: string = "";
    for (let i = 0; i < length; i++) {
        value += characters[RandomRangeInt(0, characters.length)];
    }
    return value;
};

export const RandomRange = (min: number, max: number): number => {
    return min + (RandomNormal() * (max - min));
}

export const RandomRangeInt = (min: number, max: number): number => {
    return Math.floor(RandomRange(min, max));
}

export const RandomNormal = (): number => {
    return Math.random();
}