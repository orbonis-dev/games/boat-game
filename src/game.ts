import { CanvasManager } from "./canvas/canvas-manager";
import { BoatEntity } from "./entities/boat/boat-entity";
import { FPSCounterEntity } from "./entities/debug/fps-counter-entity";
import { Entity } from "./entities/entity";
import { MapEntity } from "./entities/map/map-entity";
import { InputManager } from "./input/input-manager";
import { Texture } from "./sprites/texture";

export class Game {
    private canvas: CanvasManager;
    private input: InputManager;

    private texture: Texture;
    private entities: Entity[];

    private previousTime: number;

    constructor() {
        this.canvas = new CanvasManager(document.getElementById("game-canvas") as HTMLCanvasElement);
        this.canvas.fitToScreen = true;
        this.input = new InputManager();

        this.texture = new Texture("/assets/textures.png");
        this.entities = [];

        this.previousTime = 0;
    }

    public init(): void {
        this.render(0);

        this.texture.load().then(() => {
            this.entities.push(
                new MapEntity(this.texture, this.input),
                new BoatEntity(this.texture, this.canvas, this.input),
                new FPSCounterEntity()
            );
        });
    }

    private render = (time: number): void => {
        const delta: number = time - this.previousTime;

        this.canvas.context.clearRect(0, 0, this.canvas.size.width, this.canvas.size.height);

        this.input.update(delta);

        for (const entity of this.entities) {
            entity.update(delta);
        }

        for (const entity of this.entities) {
            entity.render(delta, this.canvas);
        }

        this.previousTime = time;
        requestAnimationFrame(this.render);
    }
}
