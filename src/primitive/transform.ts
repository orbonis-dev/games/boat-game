import { Point } from "./point";

export class Transform {
    public x: number;
    public y: number;
    public rotation: number;

    constructor(x: number, y: number, rotation: number) {
        this.x = x;
        this.y = y;
        this.rotation = rotation;
    }

    public add(value: Transform | Point): Transform {
        this.x += value.x;
        this.y += value.y;
        return this;
    }

    public multiply(value: Transform | Point): Transform {
        this.x *= value.x;
        this.y *= value.y;
        return this;
    }

    public clone(): Transform {
        return new Transform(this.x, this.y, this.rotation);
    }
}