export class Dimensions {
    public width: number;
    public height: number;

    constructor(width: number, height: number) {
        this.width = width;
        this.height = height;
    }

    public add(value: Dimensions): Dimensions {
        this.width += value.width;
        this.height += value.height;
        return this;
    }

    public multiply(value: Dimensions): Dimensions {
        this.width *= value.width;
        this.height *= value.height;
        return this;
    }

    public clone(): Dimensions {
        return new Dimensions(this.width, this.height);
    }
}
