export class Point {
    public x: number;
    public y: number;
    public z: number;

    constructor(x: number, y: number, z: number = 0) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public add(value: Point): Point {
        this.x += value.x;
        this.y += value.y;
        this.z += value.z;
        return this;
    }

    public multiply(value: Point): Point {
        this.x *= value.x;
        this.y *= value.y;
        this.z *= value.z;
        return this;
    }

    public clone(): Point {
        return new Point(this.x, this.y, this.z);
    }
}
