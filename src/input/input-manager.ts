import { Event } from "../utilities/event";

export interface KeyInputData {
    key: string;
    pressed: boolean;
    downTime: number;
    pressTime: number;
}

export class InputManager {
    public onKeyDown: Event<KeyInputData>;
    public onKeyHold: Event<KeyInputData>;
    public onKeyUp: Event<KeyInputData>;

    private data: { [key: string]: KeyInputData };

    constructor() {
        this.onKeyDown = new Event();
        this.onKeyHold = new Event();
        this.onKeyUp = new Event();
        this.data = {};

        window.addEventListener("keydown", this.keydown);
        window.addEventListener("keyup", this.keyup);
    }

    public update(delta: number): void {
        for (const key in this.data) {
            if (this.data[key].pressed) {
                this.onKeyHold.dispatch(this.data[key]);
            }
        }
    }

    private keydown = (e: KeyboardEvent): void => {
        this.validateKeyData(e.key);
        const sendEvent: boolean = !this.data[e.key].pressed;
        if (sendEvent) {
            this.data[e.key].pressed = true;
            this.data[e.key].downTime = Date.now();
            this.data[e.key].pressTime = 0;
            this.onKeyDown.dispatch(this.data[e.key]);
        }
    }

    private keyup = (e: KeyboardEvent): void => {
        this.validateKeyData(e.key);
        const sendEvent: boolean = this.data[e.key].pressed;
        if (sendEvent) {
            this.data[e.key].pressed = false;
            this.data[e.key].pressTime = Date.now() - this.data[e.key].downTime;
            this.onKeyUp.dispatch(this.data[e.key]);
        }
    }

    private validateKeyData(key: string): void {
        if (!this.data[key]) {
            this.data[key] = { key: key, pressed: false, downTime: 0, pressTime: 0 }
        }
    }
}
