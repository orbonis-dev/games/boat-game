import { CanvasManager } from "../canvas/canvas-manager";
import { Point } from "../primitive/point";
import { Dimensions } from "../primitive/rectangle";
import { Transform } from "../primitive/transform";
import { AtlasFrame } from "./atlas";

export class Sprite {
    public children: Sprite[];
    public position: Point;
    public size: Dimensions;
    public scale: number;
    public rotation: number;
    public pivot: Point;
    public offset?: Transform;

    public cacheRender: boolean;

    private id: string;
    private texture: CanvasImageSource;
    private data: AtlasFrame;

    private canvas?: CanvasManager;
    private shouldRender: boolean;

    constructor(id: string, frame: AtlasFrame, texture: CanvasImageSource) {
        this.id = id;
        this.texture = texture;
        this.data = frame;

        this.children = [];
        this.position = new Point(0, 0);
        this.size = new Dimensions(frame.spriteSourceSize.w, frame.spriteSourceSize.h);
        this.pivot = new Point(this.data.pivot.x, this.data.pivot.y);
        this.scale = 1;
        this.rotation = 0;

        this.cacheRender = false;
        this.shouldRender = true;
    }

    public setImage(id: string, frame: AtlasFrame, texture: CanvasImageSource): void {
        this.id = id;
        this.texture = texture;
        this.data = frame;
    }

    public clone(): Sprite {
        const sprite: Sprite = new Sprite(this.id, this.data, this.texture);
        sprite.position = this.position.clone();
        sprite.size = this.size.clone();
        sprite.scale = this.scale;
        sprite.rotation = this.rotation;
        sprite.children = [ ...this.children ];

        return sprite;
    }

    public render(delta: number, canvas: CanvasManager): void {
        const ctx: CanvasRenderingContext2D = canvas.context;

        const offsetPosition: Point = new Point(
            Math.floor(this.position.x),
            Math.floor(this.position.y)
        );
        const rotationRadians: number = this.rotation * (Math.PI / 180);

        ctx.save();
        ctx.translate(offsetPosition.x, offsetPosition.y);
        ctx.rotate(rotationRadians);
        if (this.offset) {
            ctx.rotate(this.offset.rotation);
            ctx.translate(this.offset.x, this.offset.y);
        }
        const scaledRectangle: Dimensions = new Dimensions(
            Math.floor(this.size.width * this.scale),
            Math.floor(this.size.height * this.scale)
        );
        if (this.cacheRender) {
            if (!this.canvas) {
                this.canvas = new CanvasManager(document.createElement("canvas"));
                this.shouldRender = true;
            }
            if (this.shouldRender) {
                this.canvas.element.width = scaledRectangle.width;
                this.canvas.element.height = scaledRectangle.height;

                const cacheCtx: CanvasRenderingContext2D = this.canvas.context;
                cacheCtx.clearRect(0, 0, scaledRectangle.width, scaledRectangle.height);
                cacheCtx.drawImage(
                    this.texture,
        
                    this.data.frame.x,
                    this.data.frame.y,
                    this.data.frame.w,
                    this.data.frame.h,
        
                    0,
                    0,
                    scaledRectangle.width,
                    scaledRectangle.height
                );
                cacheCtx.save();
                cacheCtx.translate(
                    Math.floor(scaledRectangle.width * this.pivot.x),
                    Math.floor(scaledRectangle.height * this.pivot.y)
                );
                this.children.forEach((child) => {
                    child.shouldRender = true;
                    child.render(delta, this.canvas!);
                });
                cacheCtx.restore();
                this.shouldRender = false;
                console.log(this.id, this.children.map((x) => x.id), "rendered");
            }
            ctx.drawImage(
                this.canvas.element,
                Math.floor(0 - (scaledRectangle.width * this.pivot.x)),
                Math.floor(0 - (scaledRectangle.height * this.pivot.y))
            );
        } else {
            if (this.canvas) {
                this.canvas.element.remove();
                delete this.canvas;
            }
            ctx.drawImage(
                this.texture,
    
                this.data.frame.x,
                this.data.frame.y,
                this.data.frame.w,
                this.data.frame.h,
    
                Math.floor(0 - (scaledRectangle.width * this.pivot.x)),
                Math.floor(0 - (scaledRectangle.height * this.pivot.y)),
                scaledRectangle.width,
                scaledRectangle.height
            );
            this.children.forEach((child) => child.render(delta, canvas));
        }
        ctx.restore();
    }

    public rerender(): void {
        this.shouldRender = true;
    }
}
