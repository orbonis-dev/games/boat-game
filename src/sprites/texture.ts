import { Atlas, AtlasFrame } from "./atlas";
import { Sprite } from "./sprite";

export class Texture {
    private texture?: HTMLImageElement;
    private atlas?: Atlas;

    private url: string;

    constructor(url: string) {
        this.url = url;
    }

    public async load(): Promise<boolean> {
        const imageURL: string = (this.url.endsWith(".json")) ? this.url.replace(/\.json$/, ".png") : this.url;
        const jsonURL: string = (this.url.endsWith(".json")) ? this.url : this.url.replace(/\.[a-z]+$/, ".json");

        if (!this.texture) {
            this.texture = await new Promise((resolve) => {
                const image: HTMLImageElement = document.createElement("img");
                image.addEventListener("load", async (e) => {
                    resolve(image);
                });
                image.src = imageURL;
            });
        }

        if (!this.atlas) {
            const response: Response = await fetch(jsonURL);
            if (response.ok) {
                this.atlas = await response.json();
            }
        }

        return this.texture !== undefined && this.atlas !== undefined;
    }

    public sprite(id: string): Sprite {
        if (this.texture) {
            return new Sprite(id, this.frame(id), this.texture);
        } else {
            throw new Error("This texture is not loaded. Wait for `load` to complete before trying to extract a sprite.");
        }
    }

    public changeSpriteFrame(sprite: Sprite, id: string): void {
        if (this.texture) {
            sprite.setImage(id, this.frame(id), this.texture);
        } else {
            throw new Error("This texture is not loaded. Wait for `load` to complete before trying to extract a sprite.");
        }
    }

    private frame(id: string): AtlasFrame {
        if (this.texture && this.atlas) {
            if (this.atlas.frames[id]) {
                return this.atlas?.frames[id];
            } else {
                throw new Error(`The frame ${id} does not exist in this texture.`);
            }
        } else {
            throw new Error("This texture is not loaded. Wait for `load` to complete before trying to extract a frame.");
        }
    }
}
