export interface AtlasRectangle {
    x: number;
    y: number;
    w: number;
    h: number;
}

export interface AtlasFrame {
    frame: AtlasRectangle;
    rotated: boolean;
    trimmed: boolean;
    spriteSourceSize: AtlasRectangle;
    sourceSize: AtlasRectangle;
    pivot: AtlasRectangle;
}

export interface Atlas {
    frames: { [key: string]: AtlasFrame };
    meta: {
        app: string;
        version: string;
        image: string;
        format: string;
        size: AtlasFrame;
        scale: number;
    }
}