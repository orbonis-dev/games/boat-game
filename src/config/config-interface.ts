import { Point } from "../primitive/point";

export const SupportedResolutions = [ 480, 720, 1080 ] as const;
export type SupportedResolutionsType = typeof SupportedResolutions[number];

export interface Configuration {
    resolution: SupportedResolutionsType | "auto";
    renderOrigin: Point;

    boatMoveSpeed: number;
    boatMoveAcceleration: number;
    boatMaxTurnSpeed: number;
    boatMaxTurnAngle: number;
    boatTurnAcceleration: number;
    boatPivot: Point;

    mapScale: number;
}
