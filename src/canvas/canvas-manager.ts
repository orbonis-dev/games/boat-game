import { config } from "../config";
import { SupportedResolutions, SupportedResolutionsType } from "../config/config-interface";
import { Point } from "../primitive/point";
import { Dimensions } from "../primitive/rectangle";

export class CanvasManager {
    public readonly context: CanvasRenderingContext2D;
    public readonly origin: Point;
    public readonly size: Dimensions;
    public readonly element: HTMLCanvasElement;

    public fitToScreen: boolean;

    constructor(canvas: HTMLCanvasElement, size?: Dimensions) {
        this.element = canvas;
        const context: CanvasRenderingContext2D | null = this.element.getContext("2d");
        if (context) {
            this.context = context;
        } else {
            throw new Error("Canvas context could not be retrieved.");
        }

        this.origin = new Point(0, 0);
        this.size = size ?? new Dimensions(100, 100);
        this.element.width = this.size.width;
        this.element.height = this.size.height;
        this.fitToScreen = false;

        setTimeout(() => {
            this.handleResize();
            window.addEventListener("resize", this.handleResize);
        }, 0);
    }

    private handleResize = (): void => {
        if (this.fitToScreen) {
            const automaticResolution = (): SupportedResolutionsType => {
                const windowHeight: number = window.innerHeight;
                for (let i = 0; i < SupportedResolutions.length; i++) {
                    if (windowHeight <= SupportedResolutions[i]) {
                        return SupportedResolutions[i];
                    }
                }

                return SupportedResolutions[SupportedResolutions.length - 1];
            };

            const aspectRatio: number = window.innerWidth / window.innerHeight;
            const height: number = (config.resolution === "auto") ? automaticResolution() : config.resolution;
            const width: number = Math.ceil(height * aspectRatio);
            this.element.width = width;
            this.element.height = height;

            this.origin.x = width * config.renderOrigin.x;
            this.origin.y = height * config.renderOrigin.y;
            this.size.width = width;
            this.size.height = height;
        }
    };
}
