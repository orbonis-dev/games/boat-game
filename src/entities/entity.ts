import { CanvasManager } from "../canvas/canvas-manager";
import { Game } from "../game";
import { Point } from "../primitive/point";
import { EntityComponent, EntityComponentList } from "./components/component";

export abstract class Entity {
    public components: EntityComponentList;
    public position: Point;

    constructor() {
        this.components = new EntityComponentList();
        this.position = new Point(0, 0, 0);
    }

    public update(delta: number): void {
        this.components.all().forEach((x) => x.update(delta));
    }

    public render(delta: number, canvas: CanvasManager): void {
        this.components.all().forEach((x) => x.render(delta, canvas));
    }
}
