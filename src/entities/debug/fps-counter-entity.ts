import { CanvasManager } from "../../canvas/canvas-manager";
import { Entity } from "../entity";

export class FPSCounterEntity extends Entity {
    private history: number[];
    private timeout: number;

    constructor() {
        super();

        this.history = [];
        this.timeout = 0;
    }

    public render(delta: number, canvas: CanvasManager): void {
        super.render(delta, canvas);

        this.timeout += delta;
        if (this.timeout > 1000 || this.history.length < 5) {
            this.history.push(Math.floor(1000 / delta));
            if (this.history.length > 5) {
                this.history.shift();
            }
            this.timeout = 0;
        }

        const ctx: CanvasRenderingContext2D = canvas.context;
        const fps: number = (this.history.length < 5) ? this.history[this.history.length - 1] : Math.floor(this.history.reduce((previous, current) => previous + current, 0) / this.history.length);
        ctx.font = "30px Arial";
        ctx.fillText(`${fps} FPS`, 10, 30);
    }
}