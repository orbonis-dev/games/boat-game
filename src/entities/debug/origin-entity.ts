import { CanvasManager } from "../../canvas/canvas-manager";
import { Point } from "../../primitive/point";
import { Entity } from "../entity";

export class OriginEntity extends Entity {
    public update(delta: number): void {
        super.update(delta);
    }

    public render(delta: number, canvas: CanvasManager): void {
        super.render(delta, canvas);

        const ctx: CanvasRenderingContext2D = canvas.context;
        const origin: Point = canvas.origin;

        ctx.beginPath();
        ctx.fillStyle = "#222222";
        ctx.ellipse(origin.x, origin.y, 5, 5, 0, 0, Math.PI * 2);
        ctx.fill();
        ctx.closePath();
    }
}
