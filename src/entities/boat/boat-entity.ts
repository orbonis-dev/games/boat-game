import { CanvasManager } from "../../canvas/canvas-manager";
import { config } from "../../config";
import { InputManager } from "../../input/input-manager";
import { Point } from "../../primitive/point";
import { Sprite } from "../../sprites/sprite";
import { Texture } from "../../sprites/texture";
import { Entity } from "../entity";
import { BoatInputComponent } from "./boat-input-component";

export class BoatEntity extends Entity {
    private sprite: Sprite;
    private flag: Sprite;

    constructor(texture: Texture, canvas: CanvasManager, input: InputManager) {
        super();

        this.sprite = texture.sprite("textures/ship (1)");
        this.sprite.rotation = 180;
        this.sprite.pivot = config.boatPivot;
        this.sprite.position.x = Math.floor(canvas.origin.x + this.position.x);
        this.sprite.position.y = Math.floor(canvas.origin.y + this.position.y);
        this.sprite.cacheRender = true;

        this.flag = texture.sprite("textures/flag (1)");
        this.flag.rotation = 180;
        this.flag.position.y = -10;
        this.sprite.children.push(this.flag);

        this.components.add(new BoatInputComponent(this.sprite, this.flag, canvas, input, texture));
    }

    public update(delta: number): void {
        super.update(delta);
    }

    public render(delta: number, canvas: CanvasManager): void {
        super.render(delta, canvas);

        this.sprite.render(delta, canvas);
    }
}
