import { CanvasManager } from "../../canvas/canvas-manager";
import { config } from "../../config";
import { InputManager, KeyInputData } from "../../input/input-manager";
import { Sprite } from "../../sprites/sprite";
import { Texture } from "../../sprites/texture";
import { EntityComponent } from "../components/component";

enum MovementState {
    None, Left, Right
}

export class BoatInputComponent extends EntityComponent {
    private sprite: Sprite;
    private flag: Sprite;
    private canvas: CanvasManager;
    private texture: Texture;
    private movementState: MovementState;

    private flagIds: string[];
    private flagIdIndex: number;

    constructor(sprite: Sprite, flag: Sprite, canvas: CanvasManager, input: InputManager, texture: Texture) {
        super();

        this.sprite = sprite;
        this.flag = flag;
        this.canvas = canvas;
        this.texture = texture;
        this.movementState = MovementState.None;

        this.flagIds = [
            "textures/flag (1)",
            "textures/flag (2)",
            "textures/flag (3)",
            "textures/flag (4)",
            "textures/flag (5)",
            "textures/flag (6)"
        ];
        this.flagIdIndex = 0;

        input.onKeyDown.add(this.keydown);
        input.onKeyUp.add(this.keyup);
    }

    public update(delta: number): void {
        const offset: number = delta * (config.boatMaxTurnAngle * config.boatMaxTurnSpeed);
        switch (this.movementState) {
            case MovementState.Left:
                this.sprite.rotation -= offset;
                if (this.sprite.rotation < 180 - config.boatMaxTurnAngle) {
                    this.sprite.rotation = 180 - config.boatMaxTurnAngle;
                }
                break;
            case MovementState.Right:
                this.sprite.rotation += offset;
                if (this.sprite.rotation > 180 + config.boatMaxTurnAngle) {
                    this.sprite.rotation = 180 + config.boatMaxTurnAngle;
                }
                break;
            case MovementState.None:
                if (this.sprite.rotation > 180) {
                    this.sprite.rotation -= offset;
                    if (this.sprite.rotation < 180) {
                        this.sprite.rotation = 180;
                    }
                } else if (this.sprite.rotation < 180) {
                    this.sprite.rotation += offset;
                    if (this.sprite.rotation > 180) {
                        this.sprite.rotation = 180;
                    }
                }
                break;
        }
    }

    private keydown = (e: KeyInputData): void => {
        switch (e.key.toLowerCase()) {
            case "a":
                this.movementState = MovementState.Left;
                break;
            case "d":
                this.movementState = MovementState.Right;
                break;
        }
    }

    private keyup = (e: KeyInputData): void => {
        switch (e.key.toLowerCase()) {
            case "a":
                this.movementState = MovementState.None;
                break;
            case "d":
                this.movementState = MovementState.None;
                break;
            case " ":
                this.flagIdIndex++;
                if (this.flagIdIndex >= this.flagIds.length) {
                    this.flagIdIndex = 0;
                }
                this.texture.changeSpriteFrame(this.flag, this.flagIds[this.flagIdIndex]);
                this.sprite.rerender();
                break;
        }
    }
}
