import { CanvasManager } from "../../canvas/canvas-manager";
import { RandomString } from "../../utilities/random";

export class EntityComponent {
    public update(delta: number): void {

    }
    public render(delta: number, canvas: CanvasManager): void {

    }
}

export class EntityComponentList {
    private list: { [key: string]: EntityComponent };

    constructor() {
        this.list = {};
    }

    public add(component: EntityComponent, id?: string): EntityComponentList {
        this.list[id ?? RandomString(8)] = component;
        return this;
    }

    public remove(id: string): EntityComponentList;
    public remove(component: EntityComponent): EntityComponentList;
    public remove(component: EntityComponent | string): EntityComponentList {
        if (typeof(component) === "string") {
            delete this.list[component];
        } else {
            const ids: string[] = Object.keys(this.list).filter((key) => this.list[key] === component);
            ids.forEach((id) => this.remove(id));
        }
        return this;
    }

    public get<T extends EntityComponent>(id: string): T | null;
    public get<T extends EntityComponent>(type: new (...args: any[]) => T): T | null;
    public get<T extends EntityComponent>(query: string | (new (...args: any[]) => T)): T | null {
        if (typeof(query) === "string") {
            return this.list[query] as T ?? null;
        } else {
            return Object.values(this.list).find((x) => x instanceof query) as T;
        }
    }

    public all(): EntityComponent[] {
        return Object.values(this.list);
    }
}