import { config } from "../../config";
import { InputManager, KeyInputData } from "../../input/input-manager";
import { Transform } from "../../primitive/transform";
import { EntityComponent } from "../components/component";

export class MapInputComponent extends EntityComponent {
    private transform: Transform;
    private moveSpeed: number;
    private targetMoveSpeed: number;
    private rotationSpeed: number;
    private targetRotationSpeed: number;

    constructor(input: InputManager, transform: Transform) {
        super();

        this.transform = transform;
        this.moveSpeed = 0;
        this.targetMoveSpeed = 0;
        this.rotationSpeed = 0;
        this.targetRotationSpeed = 0;

        input.onKeyHold.add(this.keydown);
        input.onKeyUp.add(this.keyup);
    }

    public update(delta: number): void {
        if (this.moveSpeed < this.targetMoveSpeed) {
            this.moveSpeed += delta * config.boatMoveAcceleration;
            if (this.moveSpeed > this.targetMoveSpeed) {
                this.moveSpeed = this.targetMoveSpeed;
            }
        } else if (this.moveSpeed > this.targetMoveSpeed) {
            this.moveSpeed -= delta * config.boatMoveAcceleration;
            if (this.moveSpeed < this.targetMoveSpeed) {
                this.moveSpeed = this.targetMoveSpeed;
            }
        }
        if (this.rotationSpeed < this.targetRotationSpeed) {
            this.rotationSpeed += delta * config.boatTurnAcceleration;
            if (this.rotationSpeed > this.targetRotationSpeed) {
                this.rotationSpeed = this.targetRotationSpeed;
            }
        } else if (this.rotationSpeed > this.targetRotationSpeed) {
            this.rotationSpeed -= delta * config.boatTurnAcceleration;
            if (this.rotationSpeed < this.targetRotationSpeed) {
                this.rotationSpeed = this.targetRotationSpeed;
            }
        }
        this.transform.rotation += this.rotationSpeed * delta * (Math.PI / 180);
        this.transform.x += this.moveSpeed * delta * Math.sin(this.transform.rotation);
        this.transform.y += this.moveSpeed * delta * Math.cos(this.transform.rotation);
    }

    private keydown = (e: KeyInputData): void => {
        switch (e.key.toLowerCase()) {
            case "w":
                this.targetMoveSpeed = -config.boatMoveSpeed;
                break;
            case "s":
                this.targetMoveSpeed = config.boatMoveSpeed;
                break;
            case "a":
                this.targetRotationSpeed = (config.boatMaxTurnAngle * config.boatMaxTurnSpeed);
                break;
            case "d":
                this.targetRotationSpeed = -(config.boatMaxTurnAngle * config.boatMaxTurnSpeed);
                break;
        }
    }

    private keyup = (e: KeyInputData): void => {
        switch (e.key.toLowerCase()) {
            case "w":
                this.targetMoveSpeed = 0;
                break;
            case "s":
                this.targetMoveSpeed = 0;
                break;
            case "a":
                this.targetRotationSpeed = 0;
                break;
            case "d":
                this.targetRotationSpeed = 0;
                break;
        }
    }
}
