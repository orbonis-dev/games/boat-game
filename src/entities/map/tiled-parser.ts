import { Point } from "../../primitive/point";
import { Dimensions } from "../../primitive/rectangle";
import { MapLayer } from "./map-layer";

export interface TiledExportLayer {
    id: number;
    name: string;
    data: number[];
    x: number;
    y: number;
    height: number;
    width: number;
    opacity: number;
    type: string;
    visible: boolean;
}

export enum TiledExportRenderOrder {
    RightDown   = "right-down",
    RightUp     = "right-up",
    LeftDown    = "left-down",
    LeftUp      = "left-up"
}

export interface TiledExportTileset {
    firstgid: number;
    source: string;
}

export interface TiledExport {
    version: string;

    type: string;
    orientation: string;
    compressionlevel: number;
    height: number;
    width: number;
    infinite: boolean;

    layers: TiledExportLayer[];
    nextlayerid: number;
    nextobjectid: number;
    renderorder: TiledExportRenderOrder;

    tilesets: TiledExportTileset[];
    tileheight: number;
    tilewidth: number;
}

export const parseTiledMap = async (url: string): Promise<MapLayer[]> => {
    const response = await fetch(url);
    if (response.ok) {
        const data: TiledExport = await response.json();
        const layers: MapLayer[] = [];

        for (const layer of data.layers) {
            const sprites: Array<string | null>[] = [];

            let i: number = 0;
            for (let y = 0; y < layer.width; y++) {
                sprites.push([]);
                for (let x = 0; x < layer.height; x++) {
                    if (layer.data[i] === 0) {
                        sprites[y].push(null);
                    } else {
                        sprites[y].push(`textures/tile_${layer.data[i].toString().padStart(2, "0")}`);
                    }
                    i++;
                }
            }

            layers.push({
                id: layer.id,
                name: layer.name,
                opacity: layer.opacity,
                position: new Point(layer.x, layer.y),
                size: new Dimensions(layer.width, layer.height),
                tileSize: new Dimensions(data.tilewidth, data.tileheight),
                visible: layer.visible,
                sprites: sprites
            });
        }
    
        return layers;
    } else {
        throw new Error(`Failed to load map data from ${url}.`);
    }
}
