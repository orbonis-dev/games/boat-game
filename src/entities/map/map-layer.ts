import { Point } from "../../primitive/point";
import { Dimensions } from "../../primitive/rectangle";

export interface MapLayer {
    id: number;
    name: string;

    sprites: Array<string | null>[];

    position: Point;
    size: Dimensions;
    tileSize: Dimensions;

    opacity: number;
    visible: boolean;
}
