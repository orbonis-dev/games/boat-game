import { CanvasManager } from "../../canvas/canvas-manager";
import { config } from "../../config";
import { InputManager } from "../../input/input-manager";
import { Point } from "../../primitive/point";
import { Dimensions } from "../../primitive/rectangle";
import { Transform } from "../../primitive/transform";
import { Sprite } from "../../sprites/sprite";
import { Texture } from "../../sprites/texture";
import { Entity } from "../entity";
import { MapInputComponent } from "./map-input-component";
import { MapLayer } from "./map-layer";
import { parseTiledMap } from "./tiled-parser";

export class MapEntity extends Entity {
    private readonly texture: Texture;
    private readonly transform: Transform;

    private canvas?: CanvasManager;
    private size: Dimensions;
    private tileSize: Dimensions;
    private sprites: (Sprite | null)[][][];

    constructor(texture: Texture, input: InputManager) {
        super();

        this.texture = texture;
        this.transform = new Transform(0, 0, 0);
        this.size = new Dimensions(0, 0);
        this.tileSize = new Dimensions(0, 0);
        this.sprites = [];

        this.components.add(new MapInputComponent(input, this.transform));

        parseTiledMap("/assets/maps/map.json").then(this.parseMapData);
    }

    public update(delta: number): void {
        super.update(delta);
    }

    public render(delta: number, canvas: CanvasManager): void {
        super.render(delta, canvas);

        if (this.canvas) {
            const ctx = canvas.context;
            ctx.save();
            ctx.translate(
                canvas.size.width / 2,
                canvas.size.height / 2
            );
            ctx.rotate(this.transform.rotation);
            ctx.translate(-this.transform.x, -this.transform.y);

            ctx.drawImage(
                this.canvas.element,
                -this.canvas.size.width / 2,
                -this.canvas.size.height / 2,
                this.canvas.size.width,
                this.canvas.size.height
            );

            ctx.restore();
        }
    }

    private parseMapData = (layers: MapLayer[]): void => {
        this.sprites = [];

        if (layers.length > 0) {
            this.size = layers[0].size.clone();
            this.tileSize = layers[0].tileSize.clone();
        }

        for (const layer of layers) {
            this.sprites.push([]);
            for (let y = 0; y < layer.sprites.length; y++) {
                this.sprites[this.sprites.length - 1].push([]);
                for (let x = 0; x < layer.sprites[y].length; x++) {
                    const spriteId: string | null = layer.sprites[y][x];
                    if (spriteId !== null) {
                        const sprite: Sprite = this.texture.sprite(spriteId);
                        sprite.scale = config.mapScale;
                        sprite.position.x = (x * layer.tileSize.width) * config.mapScale;
                        sprite.position.y = (y * layer.tileSize.height) * config.mapScale;
                        this.sprites[this.sprites.length - 1][y].push(sprite);
                    } else {
                        this.sprites[this.sprites.length - 1][y].push(null);
                    }
                }
            }
        }

        this.canvas = new CanvasManager(
            document.createElement("canvas") as HTMLCanvasElement,
            this.tileSize.clone().multiply(this.size).multiply(new Dimensions(config.mapScale, config.mapScale))
        );
        this.canvas.fitToScreen = false;

        const layerIds: number[] = Object.keys(this.sprites).map((x) => Number(x));
        for (const id of layerIds) {
            for (let y = 0; y < this.sprites[id].length; y++) {
                for (let x = 0; x < this.sprites[id][y].length; x++) {
                    const sprite: Sprite | null = this.sprites[id][y][x];
                    if (sprite) {
                        sprite.render(0, this.canvas);
                    }
                }
            }
        }
    }
}
