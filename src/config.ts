import { Configuration } from "./config/config-interface";
import { Point } from "./primitive/point";

export const config: Configuration = {
    resolution: 1080,
    renderOrigin: new Point(0.5, 0.5),

    boatMoveSpeed: 0.1,
    boatMoveAcceleration: 0.0001,
    boatMaxTurnSpeed: 0.008,
    boatMaxTurnAngle: 5,
    boatTurnAcceleration: 0.00005,
    boatPivot: new Point(0.5, 0.2),

    mapScale: 0.5
};