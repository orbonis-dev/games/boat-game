import { Game } from "./game";
import "./style.scss";

const game: Game = new Game();
game.init();
