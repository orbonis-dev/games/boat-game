const HtmlWebpackPlugin = require("html-webpack-plugin");
const { WebpackTexturePackerPlugin, PackerExporterType } = require("webpack-texture-packer");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = {
    entry: "./src/index.ts",
    mode: "development",
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, "dist")
    },
    devServer: {
        static: {
            directory: path.join(__dirname, "dist"),
        },
        compress: true,
        port: 8080,
    },
    resolve: {
        extensions: [".ts", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: [
                    {
                        loader: "ts-loader",
                        options: {
                            transpileOnly: true
                        }
                    }
                ]
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    "sass-loader"
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.ejs"
        }),
        new WebpackTexturePackerPlugin({
            rootDir: path.join(__dirname, "assets"),
            packerOptions: {
                allowRotation: false,
                allowTrim: false
            },
            items: [
                {
                    outDir: "assets",
                    name: "textures",
                    source: "textures",
                    packerOptions: { exporter: PackerExporterType.PIXI, removeFileExtension: true }
                }
            ]
        }),
        new CopyWebpackPlugin({
            patterns: [
                { from: "assets/maps", to: "assets/maps" }
            ]
        })
    ]
};